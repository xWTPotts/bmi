//
//  CalculatorBrain.swift
//  BMI Calculator
//
//  Created by Taylor Potts on 2020-07-07.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import UIKit


struct CalculatorBrain {
    var bmi: BMI?
    var advice: String?
    var color: UIColor?
    
    func getBMIValue() -> String {
        return String(format: "%.1f", bmi?.value ?? 0.0)
    }
    func getAdvice() -> String {
        return advice ?? ""
    }
    
    func getColor() -> UIColor {
        return color ?? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    mutating func calculateBMI(height: Float, weight: Float) {
        let bmiValue = (weight / (pow((height * 12), 2.0))) * 703
        
        if bmiValue < 18.5 {
            advice = "Eat more snacks"
            color = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        } else if bmiValue < 24.9 {
            advice = "Good to go"
            color = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        } else {
            advice =  "You're Fat"
            color = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        }
        
        
        bmi = BMI(value: bmiValue, advice: advice!, color: color!)
        

    }

}

//
//  bmi.swift
//  BMI Calculator
//
//  Created by Taylor Potts on 2020-07-07.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import UIKit


struct BMI {
    let value: Float
    let advice: String
    let color: UIColor
}
